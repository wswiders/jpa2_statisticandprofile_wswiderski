package com.capgemini.chess.service.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.chess.model.enumerator.Level;
import com.capgemini.chess.model.to.GameTo;
import com.capgemini.chess.model.to.UpdateProfileTo;
import com.capgemini.chess.model.to.UserProfileToShowTo;
import com.capgemini.chess.model.to.UserStatisticToShowTo;
import com.capgemini.chess.service.MatchAndStatisticServiceFacade;
import com.capgemini.chess.service.UserServieceFacade;
import com.capgemini.chess.service.exception.InputDataProfileValidationException;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class FasadesTestIntegration {

	@Autowired
	private MatchAndStatisticServiceFacade matchAndStatisticFacade;

	@Autowired
	private UserServieceFacade userServiceFacade;

	// after game statistic of winner should by updated, when method
	// showStatistic is call statistic should be correct before game player
	// id=26 have 0 matches and points, was last (4) on his level
	// and 30 in ranking
	@Test
	public void shouldShowCorrectUserStatisticAfterPlayingMatch() {
		// given
		UserStatisticToShowTo expected = giveUserStatisticToShowTo();
		// when
		matchAndStatisticFacade.saveMatchAndUpdateStatistic(26L, 29L);
		// then
		UserStatisticToShowTo actual = userServiceFacade.showStatistic(26);
		assertEquals(expected, actual);
	}

	// after profile data update correct information should be return with
	// showProfile method
	@Test
	public void shouldShowCorrectUserProfileAfterProfileDataUpdate() throws InputDataProfileValidationException {
		// given
		UserProfileToShowTo expected = giveCorrectProfileToShowTo();
		// when
		userServiceFacade.updateProfile(26L, giveCorrectUpdateProfileTo());
		// then
		UserProfileToShowTo actual = userServiceFacade.showProfile(26L);
		assertEquals(expected, actual);
	}

	@Test
	public void shouldReturnAllUserGames() {
		// when
		List<GameTo> userGames = userServiceFacade.showGames(9L);
		// then
		int actual = userGames.size();
		assertEquals(4L, actual);
	}

	private UserStatisticToShowTo giveUserStatisticToShowTo() {
		UserStatisticToShowTo expected = new UserStatisticToShowTo();
		expected.setLevel(1);
		expected.setLevelName(Level.NEWBIE);
		expected.setPoints(19370L);
		expected.setPositionInRanking(11L);
		expected.setPositionOnLevel(1L);
		expected.setMatchesLost(0L);
		expected.setMatchesPlayed(1L);
		expected.setMatchesWon(1L);
		return expected;
	}

	private UpdateProfileTo giveCorrectUpdateProfileTo() {
		UpdateProfileTo updateTo = new UpdateProfileTo();
		updateTo.setAboutMe("cos o mnie");
		updateTo.setEmail("a.b@a.pl");
		updateTo.setLifeMotto("motto zyciowe");
		updateTo.setName("Abcd");
		updateTo.setPassword("123456789");
		updateTo.setSurname("Abcde");
		return updateTo;
	}

	private UserProfileToShowTo giveCorrectProfileToShowTo() {
		UserProfileToShowTo updateTo = new UserProfileToShowTo();
		updateTo.setLogin("dui");
		updateTo.setAboutMe("cos o mnie");
		updateTo.setEmail("a.b@a.pl");
		updateTo.setLifeMotto("motto zyciowe");
		updateTo.setName("Abcd");
		updateTo.setSurname("Abcde");
		return updateTo;
	}
}
