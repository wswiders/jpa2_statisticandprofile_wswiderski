package com.capgemini.chess.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyLong;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.capgemini.chess.dao.UserDao;
import com.capgemini.chess.model.entity.ProfileEntity;
import com.capgemini.chess.model.entity.StatisticEntity;
import com.capgemini.chess.model.entity.UserEntity;
import com.capgemini.chess.model.enumerator.Level;
import com.capgemini.chess.model.to.UpdateProfileTo;
import com.capgemini.chess.model.to.UserProfileToShowTo;
import com.capgemini.chess.model.to.UserStatisticToShowTo;
import com.capgemini.chess.service.exception.InputDataProfileValidationException;

@RunWith(MockitoJUnitRunner.class)
public class UserManageServiceImplTest {

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Mock
	private UserDao userDao;

	@InjectMocks
	private UserManageServiceImpl userManageService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Mockito.when(userDao.find(anyLong())).thenReturn(getUserEntity());
	}

	// showProfile method should take correct and return data from userEntity
	// with correct information
	@Test
	public void shouldReturnCorrectProfileData() {
		// given
		UserProfileToShowTo expected = getUserToShowProfile();
		// when
		UserProfileToShowTo actual = userManageService.showProfile(1L);
		// then
		assertEquals(expected, actual);
	}

	// showStatistic method should take correct data from userEntity and return
	// object with correct information
	@Test
	public void shouldReturnCorrectStatisticData() {
		// given
		UserStatisticToShowTo expected = getUserStatistic();
		// when
		UserStatisticToShowTo actual = userManageService.showStatistic(1L);
		// then
		assertEquals(expected, actual);
	}

	// when insert surname is invalid method should throw exception with specific
	// message
	@Test
	public void shouldThrowExceptionWhenSurnameIsInvalid() throws InputDataProfileValidationException {
		// expected
		thrown.expect(InputDataProfileValidationException.class);
		thrown.expectMessage("Invalid surname - only letters alowed, first letter must be big");
		// given
		UpdateProfileTo updateProfileTo = new UpdateProfileTo();
		updateProfileTo.setSurname("dsa");
		// when
		userManageService.updateProfile(1L, updateProfileTo);
	}

	// when insert name is invalid method should throw exception with specific
	// message
	@Test
	public void shouldThrowExceptionWhenNameIsInvalid() throws InputDataProfileValidationException {
		// expected
		thrown.expect(InputDataProfileValidationException.class);
		thrown.expectMessage("Invalid name - only letters alowed, first letter must be big");
		// given
		UpdateProfileTo updateProfileTo = new UpdateProfileTo();
		updateProfileTo.setName("dsa21");
		// when
		userManageService.updateProfile(1L, updateProfileTo);
	}

	// when insert email is invalid method should throw exception with specific
	// message, to check email name and sure name must be correct
	@Test
	public void shouldThrowExceptionWhenEmailIsInvalid() throws InputDataProfileValidationException {
		// expected
		thrown.expect(InputDataProfileValidationException.class);
		thrown.expectMessage("Wrong email adress");
		// given
		UpdateProfileTo updateProfileTo = new UpdateProfileTo();
		updateProfileTo.setSurname("Nowak");
		updateProfileTo.setName("Andrzej");
		updateProfileTo.setEmail("dasdsaa");
		// when
		userManageService.updateProfile(1L, updateProfileTo);
	}

	// when insert email is null method should throw exception with specific
	// message, to check email name and sure name must be correct
	@Test
	public void shouldThrowExceptionWhenEmailIsNull() throws InputDataProfileValidationException {
		// expected
		thrown.expect(InputDataProfileValidationException.class);
		thrown.expectMessage("Wrong email adress");
		// given
		UpdateProfileTo updateProfileTo = new UpdateProfileTo();
		updateProfileTo.setSurname("Nowak");
		updateProfileTo.setName("Andrzej");
		// when
		userManageService.updateProfile(1L, updateProfileTo);
	}

	// when insert password is invalid method should throw exception with
	// specific message, to check email name, email and sure name must be correct
	@Test
	public void shouldThrowExceptionWhenPasswordIsInvalid() throws InputDataProfileValidationException {
		// expected
		thrown.expect(InputDataProfileValidationException.class);
		thrown.expectMessage("Password is to short");
		// given
		UpdateProfileTo updateProfileTo = new UpdateProfileTo();
		updateProfileTo.setSurname("Nowak");
		updateProfileTo.setName("Andrzej");
		updateProfileTo.setEmail("andrzej.nowak@capgemini.com");
		updateProfileTo.setPassword("dsads");
		// when
		userManageService.updateProfile(1L, updateProfileTo);
	}

	// when insert password is null method should throw exception with
	// specific message, to check email name, email and sure name must be correct
	@Test
	public void shouldThrowExceptionWhenPasswordIsNull() throws InputDataProfileValidationException {
		// expected
		thrown.expect(InputDataProfileValidationException.class);
		thrown.expectMessage("Password is to short");
		// given
		UpdateProfileTo updateProfileTo = new UpdateProfileTo();
		updateProfileTo.setSurname("Nowak");
		updateProfileTo.setName("Andrzej");
		updateProfileTo.setEmail("andrzej.nowak@capgemini.com");
		// when
		userManageService.updateProfile(1L, updateProfileTo);
	}

	// when all inserted data are correct should correct object be send to
	// update method
	@Test
	public void shouldCorrectObjectBeSendToUpdate() throws InputDataProfileValidationException {
		// given
		String expected = "Andrzej";
		UpdateProfileTo updateProfileTo = new UpdateProfileTo();
		updateProfileTo.setSurname("Nowak");
		updateProfileTo.setName("Andrzej");
		updateProfileTo.setEmail("andrzej.nowak@capgemini.com");
		updateProfileTo.setPassword("_213dsaJDLAdsads");
		ArgumentCaptor<UserEntity> captor = ArgumentCaptor.forClass(UserEntity.class);
		// when
		userManageService.updateProfile(1L, updateProfileTo);
		// then
		Mockito.verify(userDao).update(captor.capture());
		String actual = captor.getValue().getProfile().getName();
		assertEquals(expected, actual);
	}

	// when nullable data are set to null, it should be set to null
	// (nullable data are name, surname, lifeMotto and aboutMe)
	@Test
	public void shouldCorrectDataBeSendToUpdateWhenNullableDataAreNull() throws InputDataProfileValidationException {
		// given
		UpdateProfileTo updateProfileTo = new UpdateProfileTo();
		updateProfileTo.setSurname("Nowak");
		updateProfileTo.setEmail("andrzej.nowak@capgemini.com");
		updateProfileTo.setPassword("_213dsaJDLAdsads");
		ArgumentCaptor<UserEntity> captor = ArgumentCaptor.forClass(UserEntity.class);
		// when
		userManageService.updateProfile(1L, updateProfileTo);
		// then
		Mockito.verify(userDao).update(captor.capture());
		String actual = captor.getValue().getProfile().getName();
		assertNull(actual);
	}

	private UserEntity getUserEntity() {
		UserEntity userEntity = new UserEntity();
		userEntity.setLogin("abcd");
		userEntity.setEmail("a.b@a.pl");
		userEntity.setPassword("123456789");
		ProfileEntity profileEntity = new ProfileEntity();
		profileEntity.setName("Abcd");
		profileEntity.setSurname("Abcde");
		profileEntity.setAboutMe("cos o mnie");
		profileEntity.setLifeMotto("zycieowe motto");
		userEntity.setProfile(profileEntity);
		StatisticEntity statisticEntity = new StatisticEntity();
		statisticEntity.setLevel(Level.NEWBIE);
		statisticEntity.setPoints(150l);
		statisticEntity.setPositionOnLevel(3l);
		statisticEntity.setPositionInRanking(20L);
		statisticEntity.setMatchesPlayed(10l);
		statisticEntity.setMatchesLost(2l);
		statisticEntity.setMatchesWon(8l);
		userEntity.setStatistic(statisticEntity);
		return userEntity;
	}

	private UserStatisticToShowTo getUserStatistic() {
		UserStatisticToShowTo userStatisticTo = new UserStatisticToShowTo();
		userStatisticTo.setLevel(1);
		userStatisticTo.setLevelName(Level.NEWBIE);
		userStatisticTo.setPoints(150l);
		userStatisticTo.setPositionOnLevel(3l);
		userStatisticTo.setPositionInRanking(20L);
		userStatisticTo.setMatchesPlayed(10l);
		userStatisticTo.setMatchesLost(2l);
		userStatisticTo.setMatchesWon(8l);
		return userStatisticTo;
	}

	private UserProfileToShowTo getUserToShowProfile() {
		UserProfileToShowTo userProfileToShowTo = new UserProfileToShowTo();
		userProfileToShowTo.setLogin("abcd");
		userProfileToShowTo.setEmail("a.b@a.pl");
		userProfileToShowTo.setName("Abcd");
		userProfileToShowTo.setSurname("Abcde");
		userProfileToShowTo.setAboutMe("cos o mnie");
		userProfileToShowTo.setLifeMotto("zycieowe motto");
		return userProfileToShowTo;
	}
}
