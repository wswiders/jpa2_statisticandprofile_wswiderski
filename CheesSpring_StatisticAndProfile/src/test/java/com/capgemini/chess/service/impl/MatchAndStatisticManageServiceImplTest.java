package com.capgemini.chess.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.capgemini.chess.dao.GameDao;
import com.capgemini.chess.dao.StatisticDao;
import com.capgemini.chess.model.entity.GameEntity;
import com.capgemini.chess.model.entity.ProfileEntity;
import com.capgemini.chess.model.entity.StatisticEntity;
import com.capgemini.chess.model.entity.UserEntity;
import com.capgemini.chess.model.enumerator.Level;
import com.capgemini.chess.model.enumerator.ScaleBase;
import com.capgemini.chess.model.mapper.StatisticMapper;
import com.capgemini.chess.model.mapper.UserMapper;
import com.capgemini.chess.model.to.StatisticTo;
import com.capgemini.chess.model.to.UserTo;
import com.capgemini.chess.service.UserManageService;

@RunWith(MockitoJUnitRunner.class)
public class MatchAndStatisticManageServiceImplTest {

	@Mock
	private GameDao gameDao;

	@Mock
	private StatisticDao statisticDao;

	@Mock
	private UserManageService userService;

	@InjectMocks
	private MatchAndStatisticManageServiceImpl matchAndStatisticManageService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		// set user1 and user2 to return when try to find that user by mocked
		// service
		UserTo user1 = UserMapper.map(getUserEntity());
		UserTo user2 = UserMapper.map(getUserEntity());
		user2.getStatistic().setLevel(Level.ADVANCED);
		user2.getStatistic().setPoints(10000L);
		user2.getStatistic().setMatchesPlayed(190L);
		user2.getStatistic().setMatchesWon(188L);
		Mockito.when(userService.find(1L)).thenReturn(user1);
		Mockito.when(userService.find(2L)).thenReturn(user2);
	}

	// when players with id=1 and id=2 play match and player 1 is winner then
	// correct levels of players and correct scale base (+6) should be set in
	// GameEntity that is going to be save
	@Test
	public void shouldCorrectGameEntityBeUseInSaveMethod() {
		// given
		ArgumentCaptor<GameEntity> captor = ArgumentCaptor.forClass(GameEntity.class);
		// then
		matchAndStatisticManageService.saveMatchAndUpdateStatistic(1L, 2L);
		// then
		Mockito.verify(gameDao).save(captor.capture());
		assertEquals(Level.NEWBIE, captor.getValue().getWinnerLevel());
		assertEquals(Level.ADVANCED, captor.getValue().getLoserLevel());
		assertEquals(ScaleBase.PLUS_SIX, captor.getValue().getScaleBase());
	}

	// after save match statistics of two players should by updated
	@Test
	public void shouldMethodSaveMatchThenCalculatePointsUpdateWinnerAndLoserStatistics() {
		// when
		matchAndStatisticManageService.saveMatchAndUpdateStatistic(1L, 2L);
		// then
		Mockito.verify(statisticDao, times(2)).update(anyObject());
	}

	// after player level NEVBIE win with player level ADVANCED winner points
	// should be updated points from 0 to 2160 also player level should be up
	// and player count of wins and count of played games should increase
	@Test
	public void shouldWinnersStatisticBeUpdateCorrectlyAfterGameSave() {
		// given
		ArgumentCaptor<StatisticEntity> captor = ArgumentCaptor.forClass(StatisticEntity.class);
		// when
		matchAndStatisticManageService.saveMatchAndUpdateStatistic(1L, 2L);
		// then
		Mockito.verify(statisticDao, times(2)).update(captor.capture());
		StatisticTo winnerStatistic = StatisticMapper.map(captor.getAllValues().get(0));
		assertEquals(2160L, winnerStatistic.getPoints());
		assertEquals(11L, winnerStatistic.getMatchesPlayed());
		assertEquals(9L, winnerStatistic.getMatchesWon());
		assertEquals(Level.WEAKLING, winnerStatistic.getLevel());
	}

	// after player with level NEVBIE win with player level ADVANCED loser points
	// should be updated points from 10000 to 0 also player level should be
	// lower and player count of loser and played games should increase
	@Test
	public void shouldLoserStatisticBeUpdateCorrectlyAfterGameSave() {
		// given
		ArgumentCaptor<StatisticEntity> captor = ArgumentCaptor.forClass(StatisticEntity.class);
		// when
		matchAndStatisticManageService.saveMatchAndUpdateStatistic(1L, 2L);
		// then
		Mockito.verify(statisticDao, times(2)).update(captor.capture());
		StatisticTo loserStatistic = StatisticMapper.map(captor.getAllValues().get(1));
		assertEquals(0L, loserStatistic.getPoints());
		assertEquals(191L, loserStatistic.getMatchesPlayed());
		assertEquals(3L, loserStatistic.getMatchesLost());
		assertEquals(Level.NEWBIE, loserStatistic.getLevel());
	}

	// setAllUsersRankingPosition method should call update statistic method 2
	// times when there is only two players
	@Test
	public void shouldDoUpdatePlayerPositionTwoOfTimes() {
		// given
		StatisticEntity statistic1 = getStatisticEntity();
		StatisticEntity statistic2 = getStatisticEntity();
		Mockito.when(statisticDao.getSortedListOfPlayers())
				.thenReturn(new ArrayList<>(Arrays.asList(statistic1, statistic2)));
		// when
		matchAndStatisticManageService.setAllUsersRankingPosition();
		// then
		Mockito.verify(statisticDao, times(2)).update(anyObject());
	}

	// setAllUsersRankingPosition method should set correct position on ranking
	// list based on sorted statistic list
	@Test
	public void shouldSetCorrectPositionInPlayersRanking() {
		// given
		StatisticEntity statistic1 = getStatisticEntity();
		StatisticEntity statistic2 = getStatisticEntity();
		Mockito.when(statisticDao.getSortedListOfPlayers())
				.thenReturn(new ArrayList<>(Arrays.asList(statistic1, statistic2)));
		ArgumentCaptor<StatisticEntity> captor = ArgumentCaptor.forClass(StatisticEntity.class);
		// when
		matchAndStatisticManageService.setAllUsersRankingPosition();
		// then
		Mockito.verify(statisticDao, times(2)).update(captor.capture());
		List<StatisticEntity> statistics = captor.getAllValues();
		assertEquals(1L, statistics.get(0).getPositionInRanking());
		assertEquals(2L, statistics.get(1).getPositionInRanking());
	}

	// setAllUsersRankingPosition method should set correct position on level
	// ranking list
	@Test
	public void shouldSetCorrectPositionOnUsersLevel() {
		// given
		StatisticEntity statistic1 = getStatisticEntity();
		StatisticEntity statistic2 = getStatisticEntity();
		StatisticEntity statistic3 = getStatisticEntity();
		statistic1.setLevel(Level.WEAKLING);
		Mockito.when(statisticDao.getSortedListOfPlayers())
				.thenReturn(new ArrayList<>(Arrays.asList(statistic1, statistic2, statistic3)));
		ArgumentCaptor<StatisticEntity> captor = ArgumentCaptor.forClass(StatisticEntity.class);
		// when
		matchAndStatisticManageService.setAllUsersRankingPosition();
		// then
		Mockito.verify(statisticDao, times(3)).update(captor.capture());
		List<StatisticEntity> statistics = captor.getAllValues();
		assertEquals(1L, statistics.get(0).getPositionOnLevel());
		assertEquals(1L, statistics.get(1).getPositionOnLevel());
		assertEquals(2L, statistics.get(2).getPositionOnLevel());
	}

	// setAllUsersRankingPosition method should set correct position on level
	// ranking list when user have higher level but less points so user is on
	// lover position in sorted user list
	@Test
	public void shouldSetCorrectPositionOnUserLevelWhenUserHaveHigherLevel() {
		// given
		StatisticEntity statistic1 = getStatisticEntity();
		StatisticEntity statistic2 = getStatisticEntity();
		StatisticEntity statistic3 = getStatisticEntity();
		statistic1.setLevel(Level.WEAKLING);
		StatisticEntity statistic4 = getStatisticEntity();
		statistic4.setLevel(Level.WEAKLING);
		Mockito.when(statisticDao.getSortedListOfPlayers())
				.thenReturn(new ArrayList<>(Arrays.asList(statistic1, statistic2, statistic3, statistic4)));
		ArgumentCaptor<StatisticEntity> captor = ArgumentCaptor.forClass(StatisticEntity.class);
		// when
		matchAndStatisticManageService.setAllUsersRankingPosition();
		// then
		Mockito.verify(statisticDao, times(4)).update(captor.capture());
		List<StatisticEntity> statistics = captor.getAllValues();
		assertEquals(1L, statistics.get(0).getPositionOnLevel());
		assertEquals(1L, statistics.get(1).getPositionOnLevel());
		assertEquals(2L, statistics.get(2).getPositionOnLevel());
		assertEquals(2L, statistics.get(3).getPositionOnLevel());
	}

	private UserEntity getUserEntity() {
		UserEntity userEntity = new UserEntity();
		userEntity.setLogin("abcd");
		userEntity.setEmail("a.b@a.pl");
		userEntity.setPassword("123456789");
		ProfileEntity profileEntity = new ProfileEntity();
		profileEntity.setName("Abcd");
		profileEntity.setSurname("Abcde");
		profileEntity.setAboutMe("cos o mnie");
		profileEntity.setLifeMotto("zycieowe motto");
		userEntity.setProfile(profileEntity);
		StatisticEntity statisticEntity = getStatisticEntity();
		userEntity.setStatistic(statisticEntity);
		return userEntity;
	}

	private StatisticEntity getStatisticEntity() {
		StatisticEntity statisticEntity = new StatisticEntity();
		statisticEntity.setLevel(Level.NEWBIE);
		statisticEntity.setPoints(150l);
		statisticEntity.setPositionOnLevel(3l);
		statisticEntity.setPositionInRanking(20L);
		statisticEntity.setMatchesPlayed(10l);
		statisticEntity.setMatchesLost(2l);
		statisticEntity.setMatchesWon(8l);
		return statisticEntity;
	}
}
