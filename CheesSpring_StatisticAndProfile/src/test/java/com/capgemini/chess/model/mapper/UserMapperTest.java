package com.capgemini.chess.model.mapper;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.capgemini.chess.model.entity.ProfileEntity;
import com.capgemini.chess.model.entity.StatisticEntity;
import com.capgemini.chess.model.entity.UserEntity;
import com.capgemini.chess.model.to.ProfileTo;
import com.capgemini.chess.model.to.StatisticTo;
import com.capgemini.chess.model.to.UserTo;

public class UserMapperTest {

	@Test
	public void shouldMappingFromUserEntityToUserToBeCorrect() {
		// given
		UserEntity userEntity = new UserEntity();
		userEntity.setStatistic(new StatisticEntity());
		userEntity.setProfile(new ProfileEntity());
		// when
		UserTo userTo = UserMapper.map(userEntity);
		// then
		assertTrue(userTo.getProfile() instanceof ProfileTo);
		assertTrue(userTo.getStatistic() instanceof StatisticTo);
	}

	@Test
	public void shouldMappingFromUserToToUserEntityBeCorrect() {
		// given
		UserTo userTo = new UserTo();
		userTo.setStatistic(new StatisticTo());
		userTo.setProfile(new ProfileTo());
		// when
		UserEntity userEntity = UserMapper.map(userTo);
		// then
		assertTrue(userEntity.getProfile() instanceof ProfileEntity);
		assertTrue(userEntity.getStatistic() instanceof StatisticEntity);
	}
}
