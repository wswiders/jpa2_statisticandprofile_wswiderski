package com.capgemini.chess.dao.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.chess.dao.StatisticDao;
import com.capgemini.chess.model.entity.StatisticEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StatisticDaoImplTest {

	@Autowired
	private StatisticDao statisticDao;

	// import.sql have 30 record with users statistics, method should return all
	// statistic that are in DB
	@Test
	public void shouldReturnAllStatistic() {
		// when
		List<StatisticEntity> usersStatistics = statisticDao.getAll();
		// then
		int actual = usersStatistics.size();
		assertEquals(30, actual);
	}

	// player with id=13 have the most points, method should return that player
	// on first place
	@Test
	public void shouldReturnSortedListOfUsersStatistics() {
		// when
		List<StatisticEntity> sortedStatistics = statisticDao.getSortedListOfPlayers();
		// then
		long actual = sortedStatistics.get(0).getId();
		assertEquals(13L, actual);
	}

	// players with id=28 and id=29 have the same number of points but player 28
	// have more win games, method should return player with id=28 before other
	// player (second position)
	@Test
	public void shouldSortPlayerByWinCountWhennumberOfPointsIsTheSame() {
		// when
		List<StatisticEntity> sortedStatistics = statisticDao.getSortedListOfPlayers();
		// then
		long actual = sortedStatistics.get(1).getId();
		assertEquals(28L, actual);
	}
}
