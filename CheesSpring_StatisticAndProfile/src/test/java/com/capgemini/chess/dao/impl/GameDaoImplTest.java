package com.capgemini.chess.dao.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.chess.dao.GameDao;
import com.capgemini.chess.model.entity.GameEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class GameDaoImplTest {

	@Autowired
	private GameDao gameDao;

	// player with id=9 played in 4 games (2 wins and 2 loses), method should
	// return all 4 games
	@Test
	public void shouldReturnAllUserGames() {
		// when
		List<GameEntity> userMatches = gameDao.findUserMatches(9L);
		// then
		int actual = userMatches.size();
		assertEquals(4, actual);
	}

	// player with id=4 played in 2 games (wins), method should return only 2
	// games
	@Test
	public void shouldReturnMatchesWhenPlayerHaveOnlyWinMatches() {
		// when
		List<GameEntity> userMatches = gameDao.findUserMatches(4L);
		// then
		int actual = userMatches.size();
		assertEquals(2, actual);
	}

	// games of player with id=30 are not imported to DB, method should return
	// only 0 games
	@Test
	public void shouldNotReturnMatchesWhenPlayerDontHaveAny() {
		// when
		List<GameEntity> userMatches = gameDao.findUserMatches(30L);
		// then
		int actual = userMatches.size();
		assertEquals(0, actual);
	}
}
