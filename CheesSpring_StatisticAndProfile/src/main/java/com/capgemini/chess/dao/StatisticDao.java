package com.capgemini.chess.dao;

import java.util.List;

import com.capgemini.chess.model.entity.StatisticEntity;

public interface StatisticDao extends Dao<StatisticEntity> {

	/**
	 * Return list of {@link StatisticEntity} sorted descending by user points
	 * number and win count, finally list is sorted ascending by lost games count
	 * 
	 * @return list of sorted {@link StatisticEntity}
	 */
	public List<StatisticEntity> getSortedListOfPlayers();
}
