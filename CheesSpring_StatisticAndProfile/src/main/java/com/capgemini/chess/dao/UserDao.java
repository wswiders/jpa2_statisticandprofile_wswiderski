package com.capgemini.chess.dao;

import com.capgemini.chess.model.entity.UserEntity;

public interface UserDao extends Dao<UserEntity> {

}
