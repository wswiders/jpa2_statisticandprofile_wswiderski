package com.capgemini.chess.dao;

import java.util.List;

import com.capgemini.chess.model.entity.GameEntity;

public interface GameDao extends Dao<GameEntity> {

	/**
	 * Find list of {@link GameEntity} where winner or loser ID is equal to
	 * userId
	 * 
	 * @param userId
	 *            user ID number
	 * @return users list of {@link GameEntity}
	 */
	public List<GameEntity> findUserMatches(Long userId);
}
