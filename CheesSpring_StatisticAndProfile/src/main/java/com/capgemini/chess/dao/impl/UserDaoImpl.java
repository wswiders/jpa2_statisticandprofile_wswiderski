package com.capgemini.chess.dao.impl;

import org.springframework.stereotype.Repository;

import com.capgemini.chess.dao.UserDao;
import com.capgemini.chess.model.entity.UserEntity;

@Repository
public class UserDaoImpl extends AbstractDao<UserEntity> implements UserDao {

}