package com.capgemini.chess.dao;

import java.util.List;

import com.capgemini.chess.model.entity.AbstractEntity;

public interface Dao<T extends AbstractEntity> {

	/**
	 * Find entity by ID
	 * 
	 * @param id
	 *            entity ID number
	 * @return
	 */
	public T find(Long id);

	/**
	 * Save entity
	 * 
	 * @param entity
	 *            entity to save
	 */
	public void save(T entity);

	/**
	 * Update existing entity
	 * 
	 * @param entity
	 *            entity to update
	 */
	public void update(T entity);

	/**
	 * Find all entity
	 * 
	 * @return list of entity
	 */
	public List<T> getAll();
}
