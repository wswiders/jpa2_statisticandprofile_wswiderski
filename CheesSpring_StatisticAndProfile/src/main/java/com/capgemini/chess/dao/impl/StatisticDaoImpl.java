package com.capgemini.chess.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.capgemini.chess.dao.StatisticDao;
import com.capgemini.chess.model.entity.StatisticEntity;

@Repository
public class StatisticDaoImpl extends AbstractDao<StatisticEntity> implements StatisticDao {

	@Override
	public List<StatisticEntity> getSortedListOfPlayers() {
		TypedQuery<StatisticEntity> query = em.createQuery(
				"SELECT s FROM StatisticEntity s ORDER BY s.points DESC, s.matchesWon DESC, s.matchesLost",
				StatisticEntity.class);
		return query.getResultList();
	}
}
