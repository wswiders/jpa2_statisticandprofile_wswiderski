package com.capgemini.chess.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.capgemini.chess.dao.GameDao;
import com.capgemini.chess.model.entity.GameEntity;

@Repository
public class GameDaoImpl extends AbstractDao<GameEntity> implements GameDao {

	@Override
	public List<GameEntity> findUserMatches(Long userId) {
		TypedQuery<GameEntity> query = em.createQuery(
				"SELECT g FROM GameEntity g WHERE g.loser.id = :userId OR g.winner.id = :userId", GameEntity.class);
		List<GameEntity> results = query.setParameter("userId", userId).getResultList();
		return results;
	}
}
