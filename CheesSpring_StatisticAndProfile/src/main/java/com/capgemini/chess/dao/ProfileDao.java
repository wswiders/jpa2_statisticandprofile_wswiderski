package com.capgemini.chess.dao;

import com.capgemini.chess.model.entity.ProfileEntity;

public interface ProfileDao extends Dao<ProfileEntity> {

}
