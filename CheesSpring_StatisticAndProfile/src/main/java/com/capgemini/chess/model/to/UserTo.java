package com.capgemini.chess.model.to;

public class UserTo {

	private Long id;
	private Long version;
	private String login;
	private ProfileTo profile;
	private String email;
	private String password;
	private StatisticTo statistic;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public ProfileTo getProfile() {
		return profile;
	}

	public void setProfile(ProfileTo profile) {
		this.profile = profile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public StatisticTo getStatistic() {
		return statistic;
	}

	public void setStatistic(StatisticTo statistic) {
		this.statistic = statistic;
	}
}
