package com.capgemini.chess.model.mapper;

import com.capgemini.chess.model.entity.UserEntity;
import com.capgemini.chess.model.to.UserTo;

public class UserMapper {

	public static UserTo map(UserEntity entity) {
		if (entity != null) {
			UserTo to = new UserTo();
			to.setId(entity.getId());
			to.setVersion(entity.getVersion());
			to.setLogin(entity.getLogin());
			to.setEmail(entity.getEmail());
			to.setPassword(entity.getPassword());
			to.setProfile(ProfileMapper.map(entity.getProfile()));
			to.setStatistic(StatisticMapper.map(entity.getStatistic()));
			return to;
		} else {
			return null;
		}
	}

	public static UserEntity map(UserTo to) {
		if (to != null) {
			UserEntity entity = new UserEntity();
			entity.setId(to.getId());
			entity.setVersion(to.getVersion());
			entity.setLogin(to.getLogin());
			entity.setEmail(to.getEmail());
			entity.setPassword(to.getPassword());
			entity.setProfile(ProfileMapper.map(to.getProfile()));
			entity.setStatistic(StatisticMapper.map(to.getStatistic()));
			return entity;
		} else {
			return null;
		}
	}
}
