package com.capgemini.chess.model.mapper;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.chess.model.entity.GameEntity;
import com.capgemini.chess.model.to.GameTo;

public class GameMapper {

	public static GameTo map(GameEntity entity) {
		GameTo to = new GameTo();
		to.setId(entity.getId());
		to.setVersion(entity.getVersion());
		to.setLoser(UserMapper.map(entity.getLoser()));
		to.setWinner(UserMapper.map(entity.getWinner()));
		to.setLoserLevel(entity.getLoserLevel());
		to.setWinnerLevel(entity.getWinnerLevel());
		to.setScaleBase(entity.getScaleBase());
		return to;
	}

	public static GameEntity map(GameTo to) {
		GameEntity entity = new GameEntity();
		entity.setId(to.getId());
		entity.setVersion(to.getVersion());
		entity.setLoser(UserMapper.map(to.getLoser()));
		entity.setWinner(UserMapper.map(to.getWinner()));
		entity.setLoserLevel(to.getLoserLevel());
		entity.setWinnerLevel(to.getWinnerLevel());
		entity.setScaleBase(to.getScaleBase());
		return entity;
	}

	public static List<GameTo> map2To(List<GameEntity> entities) {
		ArrayList<GameTo> tos = new ArrayList<>();
		for (GameEntity gameEntity : entities) {
			tos.add(GameMapper.map(gameEntity));
		}
		return tos;
	}

	public static List<GameEntity> map2Entity(List<GameTo> tos) {
		ArrayList<GameEntity> entities = new ArrayList<>();
		for (GameTo gameTo : tos) {
			entities.add(GameMapper.map(gameTo));
		}
		return entities;
	}
}
