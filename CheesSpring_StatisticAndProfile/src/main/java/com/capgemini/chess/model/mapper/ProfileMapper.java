package com.capgemini.chess.model.mapper;

import com.capgemini.chess.model.entity.ProfileEntity;
import com.capgemini.chess.model.to.ProfileTo;

public class ProfileMapper {

	public static ProfileTo map(ProfileEntity entity) {
		if (entity != null) {
			ProfileTo to = new ProfileTo();
			to.setId(entity.getId());
			to.setVersion(entity.getVersion());
			to.setAboutMe(entity.getAboutMe());
			to.setLifeMotto(entity.getLifeMotto());
			to.setName(entity.getName());
			to.setSurname(entity.getSurname());
			return to;
		} else {
			return null;
		}
	}

	public static ProfileEntity map(ProfileTo to) {
		if (to != null) {
			ProfileEntity entity = new ProfileEntity();
			entity.setId(to.getId());
			entity.setVersion(to.getVersion());
			entity.setAboutMe(to.getAboutMe());
			entity.setLifeMotto(to.getLifeMotto());
			entity.setName(to.getName());
			entity.setSurname(to.getSurname());
			return entity;
		} else {
			return null;
		}
	}
}
