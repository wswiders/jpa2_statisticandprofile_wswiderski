package com.capgemini.chess.model.enumerator;

public enum Level {

	NEWBIE(new int[] { 1, 0, 0, 0 }), 
	WEAKLING(new int[] { 2, 301, 5, 8 }), 
	BEGINNER(new int[] { 3, 601, 20, 16 }), 
	EXPERIENCED_BEGINNER(new int[] { 4, 1201, 45, 24 }), 
	MIDDLEBROW(new int[] { 5, 2401, 80, 32 }), 
	EXPERIENCED_MIDDLEBROW(new int[] { 6, 4801, 125, 40 }), 
	ADVANCED(new int[] { 7, 9601, 180, 48 }), 
	PROFESSIONAL(new int[] { 8, 19201, 245, 56 }), 
	MASTER(new int[] { 9, 31401, 320, 64 }), 
	CHUCK_NORRIS_OF_CHESS(new int[] { 10, 76801, 405, 72 });

	private int[] levelData;

	private Level(int[] levelData) {
		this.levelData = levelData;
	}

	public int[] getLevelData() {
		return levelData;
	}
	
	public static Level findLevel(int lvl){
		for (Level level : Level.values()) {
			if(level.getLevelData()[0]==lvl){
				return level;
			}
		}
		return null;
	}
}
