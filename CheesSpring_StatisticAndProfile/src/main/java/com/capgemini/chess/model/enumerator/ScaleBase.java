package com.capgemini.chess.model.enumerator;

public enum ScaleBase {

	MINUS_NINE(new int[] { -9, 1, 1 }),
	MINUS_EIGHT(new int[] { -8, 1, 1 }),
	MINUS_SEVEN(new int[] { -7, 1, 1 }),
	MINUS_SIX(new int[] { -6, 1, 1 }),
	MINUS_FIVE(new int[] { -5, 1, 1 }),
	MINUS_FOUR(new int[] { -4, 2, 1 }),
	MINUS_THREE(new int[] { -3, 5, 3 }),
	MINUS_TWO(new int[] { -2, 10,7 }), 
	MINUS_ONE(new int[] { -1, 20, 15 }), 
	ZERO(new int[] { 0, 40, 30 }), 
	PLUS_NINE(new int[] { 9, 20480, 15360 }), 
	PLUS_EIGHT(new int[] { 8, 10240, 7680 }),
	PLUS_SEVEN(new int[] { 7, 5120, 3840 }),
	PLUS_SIX(new int[] { 6, 2560, 1920 }), 
	PLUS_FIVE(new int[] { 5, 1280, 960 }), 
	PLUS_FOUR(new int[] { 4, 640, 480 }), 
	PLUS_THREE(new int[] { 3, 320, 240 }), 
	PLUS_TWO(new int[] { 2, 160, 120 }), 
	PLUS_ONE(new int[] { 1, 80, 60 });

	private int[] base;

	private ScaleBase(int[] base) {
		this.base = base;
	}

	public int[] getBase() {
		return base;
	}

	public static ScaleBase getScaleBaseByNumber(int number) {
		for (ScaleBase base : ScaleBase.values()) {
			if (base.getBase()[0] == number) {
				return base;
			}
		}
		return null;
	}
}
