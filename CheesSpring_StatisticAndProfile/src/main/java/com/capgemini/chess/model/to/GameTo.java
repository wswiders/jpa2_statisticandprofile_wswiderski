package com.capgemini.chess.model.to;

import com.capgemini.chess.model.enumerator.Level;
import com.capgemini.chess.model.enumerator.ScaleBase;

public class GameTo {

	private Long id;
	private Long version;
	private UserTo loser;
	private UserTo winner;
	private Level loserLevel;
	private Level winnerLevel;
	private ScaleBase scaleBase;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public UserTo getLoser() {
		return loser;
	}

	public void setLoser(UserTo loser) {
		this.loser = loser;
	}

	public UserTo getWinner() {
		return winner;
	}

	public void setWinner(UserTo winner) {
		this.winner = winner;
	}

	public Level getLoserLevel() {
		return loserLevel;
	}

	public void setLoserLevel(Level loserLevel) {
		this.loserLevel = loserLevel;
	}

	public Level getWinnerLevel() {
		return winnerLevel;
	}

	public void setWinnerLevel(Level winnerLevel) {
		this.winnerLevel = winnerLevel;
	}

	public ScaleBase getScaleBase() {
		return scaleBase;
	}

	public void setScaleBase(ScaleBase scaleBase) {
		this.scaleBase = scaleBase;
	}
}
