package com.capgemini.chess.model.to;

import com.capgemini.chess.model.enumerator.Level;

public class UserStatisticToShowTo {

	private int level;
	private Level levelName;
	private long points;
	private long positionOnLevel;
	private long positionInRanking;
	private long matchesPlayed;
	private long matchesWon;
	private long matchesLost;

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public Level getLevelName() {
		return levelName;
	}

	public void setLevelName(Level levelName) {
		this.levelName = levelName;
	}

	public long getPoints() {
		return points;
	}

	public void setPoints(long points) {
		this.points = points;
	}

	public long getPositionOnLevel() {
		return positionOnLevel;
	}

	public void setPositionOnLevel(long positionOnLevel) {
		this.positionOnLevel = positionOnLevel;
	}

	public long getMatchesPlayed() {
		return matchesPlayed;
	}

	public void setMatchesPlayed(long matchesPlayed) {
		this.matchesPlayed = matchesPlayed;
	}

	public long getMatchesWon() {
		return matchesWon;
	}

	public void setMatchesWon(long matchesWon) {
		this.matchesWon = matchesWon;
	}

	public long getMatchesLost() {
		return matchesLost;
	}

	public void setMatchesLost(long matchesLost) {
		this.matchesLost = matchesLost;
	}

	public long getPositionInRanking() {
		return positionInRanking;
	}

	public void setPositionInRanking(long positionInRanking) {
		this.positionInRanking = positionInRanking;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + level;
		result = prime * result + ((levelName == null) ? 0 : levelName.hashCode());
		result = prime * result + (int) (matchesLost ^ (matchesLost >>> 32));
		result = prime * result + (int) (matchesPlayed ^ (matchesPlayed >>> 32));
		result = prime * result + (int) (matchesWon ^ (matchesWon >>> 32));
		result = prime * result + (int) (points ^ (points >>> 32));
		result = prime * result + (int) (positionInRanking ^ (positionInRanking >>> 32));
		result = prime * result + (int) (positionOnLevel ^ (positionOnLevel >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserStatisticToShowTo other = (UserStatisticToShowTo) obj;
		if (level != other.level)
			return false;
		if (levelName != other.levelName)
			return false;
		if (matchesLost != other.matchesLost)
			return false;
		if (matchesPlayed != other.matchesPlayed)
			return false;
		if (matchesWon != other.matchesWon)
			return false;
		if (points != other.points)
			return false;
		if (positionInRanking != other.positionInRanking)
			return false;
		if (positionOnLevel != other.positionOnLevel)
			return false;
		return true;
	}
}
