package com.capgemini.chess.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.capgemini.chess.model.enumerator.Level;

@Entity
@Table(name = "user_statistic")
public class StatisticEntity extends AbstractEntity {

	@Column(nullable = false, columnDefinition = "VARCHAR(255) DEFAULT 'NEWBIE'")
	@Enumerated(EnumType.STRING)
	private Level level;

	@Column(nullable = false)
	@ColumnDefault("0")
	private long points;

	@Column(nullable = false)
	@ColumnDefault("0")
	private long matchesPlayed;

	@Column(nullable = false)
	@ColumnDefault("0")
	private long matchesWon;

	@Column(nullable = false)
	@ColumnDefault("0")
	private long matchesLost;
	
	@Column
	@ColumnDefault("0")
	private long positionOnLevel;
	
	@Column
	@ColumnDefault("0")
	private long positionInRanking;

	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	public long getPoints() {
		return points;
	}

	public void setPoints(long points) {
		this.points = points;
	}

	public long getPositionOnLevel() {
		return positionOnLevel;
	}

	public void setPositionOnLevel(long positionOnLevel) {
		this.positionOnLevel = positionOnLevel;
	}

	public long getMatchesPlayed() {
		return matchesPlayed;
	}

	public void setMatchesPlayed(long matchesPlayed) {
		this.matchesPlayed = matchesPlayed;
	}

	public long getMatchesWon() {
		return matchesWon;
	}

	public void setMatchesWon(long matchesWon) {
		this.matchesWon = matchesWon;
	}

	public long getMatchesLost() {
		return matchesLost;
	}

	public void setMatchesLost(long matchesLost) {
		this.matchesLost = matchesLost;
	}

	public long getPositionInRanking() {
		return positionInRanking;
	}

	public void setPositionInRanking(long positionInRanking) {
		this.positionInRanking = positionInRanking;
	}
}
