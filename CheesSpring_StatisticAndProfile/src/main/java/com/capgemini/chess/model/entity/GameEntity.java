package com.capgemini.chess.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.capgemini.chess.model.enumerator.Level;
import com.capgemini.chess.model.enumerator.ScaleBase;

@Entity
@Table(name = "game")
public class GameEntity extends AbstractEntity {

	@ManyToOne
	@JoinColumn(nullable = false)
	private UserEntity loser;

	@ManyToOne
	@JoinColumn(nullable = false)
	private UserEntity winner;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Level loserLevel;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Level winnerLevel;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private ScaleBase scaleBase;

	public UserEntity getLoser() {
		return loser;
	}

	public void setLoser(UserEntity loser) {
		this.loser = loser;
	}

	public UserEntity getWinner() {
		return winner;
	}

	public void setWinner(UserEntity winner) {
		this.winner = winner;
	}

	public Level getLoserLevel() {
		return loserLevel;
	}

	public void setLoserLevel(Level loserLevel) {
		this.loserLevel = loserLevel;
	}

	public Level getWinnerLevel() {
		return winnerLevel;
	}

	public void setWinnerLevel(Level winnerLevel) {
		this.winnerLevel = winnerLevel;
	}

	public ScaleBase getScaleBase() {
		return scaleBase;
	}

	public void setScaleBase(ScaleBase scaleBase) {
		this.scaleBase = scaleBase;
	}

}
