package com.capgemini.chess.model.mapper;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.chess.model.entity.StatisticEntity;
import com.capgemini.chess.model.to.StatisticTo;

public class StatisticMapper {

	public static StatisticEntity map(StatisticTo to) {
		if (to != null) {
			StatisticEntity entity = new StatisticEntity();
			entity.setId(to.getId());
			entity.setVersion(to.getVersion());
			entity.setPositionInRanking(to.getPositionInRanking());
			entity.setLevel(to.getLevel());
			entity.setMatchesLost(to.getMatchesLost());
			entity.setMatchesPlayed(to.getMatchesPlayed());
			entity.setMatchesWon(to.getMatchesWon());
			entity.setPoints(to.getPoints());
			entity.setPositionOnLevel(to.getPositionOnLevel());
			return entity;
		} else {
			return null;
		}
	}

	public static StatisticTo map(StatisticEntity entity) {
		if (entity != null) {
			StatisticTo to = new StatisticTo();
			to.setId(entity.getId());
			to.setVersion(entity.getVersion());
			to.setPositionInRanking(entity.getPositionInRanking());
			to.setLevel(entity.getLevel());
			to.setMatchesLost(entity.getMatchesLost());
			to.setMatchesPlayed(entity.getMatchesPlayed());
			to.setMatchesWon(entity.getMatchesWon());
			to.setPoints(entity.getPoints());
			to.setPositionOnLevel(entity.getPositionOnLevel());
			return to;
		} else {
			return null;
		}
	}

	public static List<StatisticTo> map2To(List<StatisticEntity> entities) {
		ArrayList<StatisticTo> tos = new ArrayList<>();
		for (StatisticEntity statisticEntity : entities) {
			tos.add(StatisticMapper.map(statisticEntity));
		}
		return tos;
	}

	public static List<StatisticEntity> map2Entity(List<StatisticTo> tos) {
		ArrayList<StatisticEntity> entities = new ArrayList<>();
		for (StatisticTo statisticTo : tos) {
			entities.add(StatisticMapper.map(statisticTo));
		}
		return entities;
	}
}
