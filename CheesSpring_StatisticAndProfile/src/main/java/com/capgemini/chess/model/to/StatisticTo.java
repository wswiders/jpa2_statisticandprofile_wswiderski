package com.capgemini.chess.model.to;

import com.capgemini.chess.model.enumerator.Level;

public class StatisticTo {

	private Long id;
	private Long version;
	private Level level;
	private long points;
	private long matchesPlayed;
	private long matchesWon;
	private long matchesLost;
	private long positionOnLevel;
	private long positionInRanking;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	public long getPoints() {
		return points;
	}

	public void setPoints(long points) {
		this.points = points;
	}

	public long getPositionOnLevel() {
		return positionOnLevel;
	}

	public void setPositionOnLevel(long positionOnLevel) {
		this.positionOnLevel = positionOnLevel;
	}

	public long getMatchesPlayed() {
		return matchesPlayed;
	}

	public void setMatchesPlayed(long matchesPlayed) {
		this.matchesPlayed = matchesPlayed;
	}

	public long getMatchesWon() {
		return matchesWon;
	}

	public void setMatchesWon(long matchesWon) {
		this.matchesWon = matchesWon;
	}

	public long getMatchesLost() {
		return matchesLost;
	}

	public void setMatchesLost(long matchesLost) {
		this.matchesLost = matchesLost;
	}

	public long getPositionInRanking() {
		return positionInRanking;
	}

	public void setPositionInRanking(long positionInRanking) {
		this.positionInRanking = positionInRanking;
	}
}
