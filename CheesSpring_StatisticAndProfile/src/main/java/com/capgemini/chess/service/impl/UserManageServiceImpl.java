package com.capgemini.chess.service.impl;

import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.chess.dao.GameDao;
import com.capgemini.chess.dao.UserDao;
import com.capgemini.chess.model.mapper.GameMapper;
import com.capgemini.chess.model.mapper.UserMapper;
import com.capgemini.chess.model.to.GameTo;
import com.capgemini.chess.model.to.UserProfileToShowTo;
import com.capgemini.chess.model.to.StatisticTo;
import com.capgemini.chess.model.to.UpdateProfileTo;
import com.capgemini.chess.model.to.UserStatisticToShowTo;
import com.capgemini.chess.model.to.UserTo;
import com.capgemini.chess.service.UserManageService;
import com.capgemini.chess.service.exception.InputDataProfileValidationException;

@Service
@Transactional
public class UserManageServiceImpl implements UserManageService {

	private static final String EMAIL_PATTERN = "[a-zA-Z._]{1,}[@][a-z]{1,}[.][a-z]{2,3}";
	private static final int MIN_PASSWORD_LENTH = 8;
	private static final String NAME_PATTERN = "[A-Z][a-z]{1,}";
	private static final String SURNAME_PATTERN = "[A-Z][a-z]{1,}";

	@Autowired
	private UserDao userDao;

	@Autowired
	private GameDao gameDao;

	@Override
	public UserTo find(Long id) {
		return UserMapper.map(userDao.find(id));
	}

	@Override
	public UserStatisticToShowTo showStatistic(Long userId) {
		UserStatisticToShowTo userStatisticTo = new UserStatisticToShowTo();
		UserTo user = this.find(userId);
		StatisticTo statistics = user.getStatistic();
		setStatisticsOnTo(userStatisticTo, statistics);
		return userStatisticTo;
	}

	@Override
	public void updateProfile(Long userId, UpdateProfileTo updatedProfile) throws InputDataProfileValidationException {
		UserTo user = this.find(userId);
		updateUserProfile(user, updatedProfile);
		userDao.update(UserMapper.map(user));
	}

	@Override
	public UserProfileToShowTo showProfile(Long userId) {
		UserTo user = this.find(userId);
		return setProfileToShowData(user);
	}

	@Override
	public List<GameTo> showGames(Long userId) {
		return GameMapper.map2To(gameDao.findUserMatches(userId));
	}

	private void setStatisticsOnTo(UserStatisticToShowTo userRankingTo, StatisticTo statistics) {
		userRankingTo.setLevel(statistics.getLevel().getLevelData()[0]);
		userRankingTo.setLevelName(statistics.getLevel());
		userRankingTo.setPoints(statistics.getPoints());
		userRankingTo.setPositionOnLevel(statistics.getPositionOnLevel());
		userRankingTo.setMatchesPlayed(statistics.getMatchesPlayed());
		userRankingTo.setMatchesWon(statistics.getMatchesWon());
		userRankingTo.setMatchesLost(statistics.getMatchesLost());
		userRankingTo.setPositionInRanking(statistics.getPositionInRanking());
	}

	private void updateUserProfile(UserTo user, UpdateProfileTo updatedProfile) throws InputDataProfileValidationException {
		validateInput(updatedProfile);
		user.getProfile().setName(updatedProfile.getName());
		user.getProfile().setSurname(updatedProfile.getSurname());
		user.getProfile().setLifeMotto(updatedProfile.getLifeMotto());
		user.getProfile().setAboutMe(updatedProfile.getAboutMe());
		user.setEmail(updatedProfile.getEmail());
		user.setPassword(updatedProfile.getPassword());
	}

	private UserProfileToShowTo setProfileToShowData(UserTo user) {
		UserProfileToShowTo profileToShowTo = new UserProfileToShowTo();
		profileToShowTo.setName(user.getProfile().getName());
		profileToShowTo.setSurname(user.getProfile().getSurname());
		profileToShowTo.setAboutMe(user.getProfile().getAboutMe());
		profileToShowTo.setLifeMotto(user.getProfile().getLifeMotto());
		profileToShowTo.setEmail(user.getEmail());
		profileToShowTo.setLogin(user.getLogin());
		return profileToShowTo;
	}

	private void validateInput(UpdateProfileTo to) throws InputDataProfileValidationException {
		validateName(to.getName());
		validateSurname(to.getSurname());
		validateEmail(to.getEmail());
		validatePassword(to.getPassword());
	}

	private void validateSurname(String surname) throws InputDataProfileValidationException {
		if (surname != null && !Pattern.matches(SURNAME_PATTERN, surname)) {
			throw new InputDataProfileValidationException(
					"Invalid surname - only letters alowed, first letter must be big");
		}
	}

	private void validateName(String name) throws InputDataProfileValidationException {
		if (name != null && !Pattern.matches(NAME_PATTERN, name)) {
			throw new InputDataProfileValidationException("Invalid name - only letters alowed, first letter must be big");
		}
	}

	private void validatePassword(String password) throws InputDataProfileValidationException {
		if (password == null || password.length() < MIN_PASSWORD_LENTH) {
			throw new InputDataProfileValidationException("Password is to short");
		}
	}

	private void validateEmail(String email) throws InputDataProfileValidationException {
		if (email == null || !Pattern.matches(EMAIL_PATTERN, email)) {
			throw new InputDataProfileValidationException("Wrong email adress");
		}
	}
}
