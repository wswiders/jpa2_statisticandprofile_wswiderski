package com.capgemini.chess.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.chess.service.MatchAndStatisticManageService;
import com.capgemini.chess.service.MatchAndStatisticServiceFacade;

@Service
public class MatchAndStatisticServiceFacadeImpl implements MatchAndStatisticServiceFacade {

	@Autowired
	private MatchAndStatisticManageService matchAndStatisticManageService;

	@Override
	public void saveMatchAndUpdateStatistic(Long winnerId, Long loserId) {
		matchAndStatisticManageService.saveMatchAndUpdateStatistic(winnerId, loserId);
	}

	@Override
	public void setAllUsersRankingPosition() {
		matchAndStatisticManageService.setAllUsersRankingPosition();
	}
}
