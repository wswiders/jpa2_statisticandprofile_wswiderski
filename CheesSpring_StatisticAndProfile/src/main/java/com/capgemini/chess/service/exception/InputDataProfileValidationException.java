package com.capgemini.chess.service.exception;

public class InputDataProfileValidationException extends Exception {

	private static final long serialVersionUID = 1L;

	public InputDataProfileValidationException(String msg) {
		super(msg);
	}
}
