package com.capgemini.chess.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.chess.model.to.UserProfileToShowTo;
import com.capgemini.chess.model.to.GameTo;
import com.capgemini.chess.model.to.UpdateProfileTo;
import com.capgemini.chess.model.to.UserStatisticToShowTo;
import com.capgemini.chess.service.MatchAndStatisticManageService;
import com.capgemini.chess.service.UserManageService;
import com.capgemini.chess.service.UserServieceFacade;
import com.capgemini.chess.service.exception.InputDataProfileValidationException;

@Service
@Transactional
public class UserServieceFacadeImpl implements UserServieceFacade {

	@Autowired
	private UserManageService userManageService;

	@Autowired
	private MatchAndStatisticManageService matchAndStatisticService;

	@Override
	public void updateProfile(long userId, UpdateProfileTo updateProfileTo) throws InputDataProfileValidationException {
		userManageService.updateProfile(userId, updateProfileTo);
	}

	@Override
	public UserStatisticToShowTo showStatistic(long userId) {
		matchAndStatisticService.setAllUsersRankingPosition();
		return userManageService.showStatistic(userId);
	}

	@Override
	public UserProfileToShowTo showProfile(long userId) {
		return userManageService.showProfile(userId);
	}

	@Override
	public List<GameTo> showGames(Long userId) {
		return userManageService.showGames(userId);
	}
}
