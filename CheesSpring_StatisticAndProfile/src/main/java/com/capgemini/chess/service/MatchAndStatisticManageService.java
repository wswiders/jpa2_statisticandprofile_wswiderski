package com.capgemini.chess.service;

public interface MatchAndStatisticManageService {

	/**
	 * Method save match and update players statistics. Saved game is between
	 * users with insert ID. Game is save as {@link GameEntity}, next updated is
	 * number of games, number of win/loss, points and level of the player. Last
	 * step is to set correct user position in ranking and player position on
	 * level player actual have. This method use {@link UserManageService} to
	 * find player by his ID.
	 * 
	 * @param winnerId
	 *            ID number of winner
	 * @param loserId
	 *            ID number of loser
	 */
	public void saveMatchAndUpdateStatistic(Long winnerId, Long loserId);

	/**
	 * Set all players ranking position and position on level player actual
	 * have. Position is set based on player points count, then number of win
	 * games and finally number of lost games.
	 */
	public void setAllUsersRankingPosition();
}