package com.capgemini.chess.service;

import com.capgemini.chess.model.to.UserProfileToShowTo;

import java.util.List;

import com.capgemini.chess.model.to.GameTo;
import com.capgemini.chess.model.to.UpdateProfileTo;
import com.capgemini.chess.model.to.UserStatisticToShowTo;
import com.capgemini.chess.service.exception.InputDataProfileValidationException;

public interface UserServieceFacade {

	/**
	 * Method to update user profile data. User can update email, password,
	 * name, surname, life motto, and about me note. Email, password, name and
	 * surname are validated, when data are incorrect method throw
	 * {@link InputDataProfileValidationException}
	 * 
	 * @param userId
	 *            user ID number
	 * @param updatedProfile
	 *            form with variables to update
	 * @throws InputDataProfileValidationException
	 */
	public void updateProfile(long userId, UpdateProfileTo updateProfileTo) throws InputDataProfileValidationException;

	/**
	 * Find user by ID and return user statistic using
	 * {@link UserStatisticToShowTo}
	 * 
	 * @param userId
	 *            user ID number
	 * @return {@link UserStatisticToShowTo} when ID in DB
	 */
	public UserStatisticToShowTo showStatistic(long userId);

	/**
	 * Find user by ID and show his profile data in {@link UserProfileToShowTo}.
	 * User can see login, email, name, surname, life motto and about me note
	 * 
	 * @param userId
	 *            user ID number
	 * @return {@link UserProfileToShowTo} with information from DB
	 */
	public UserProfileToShowTo showProfile(long userId);

	/**
	 * Find all games that player participate in and return them in a list. Next
	 * that list is map to {@link GameTo}
	 * 
	 * @param userId
	 *            user ID number
	 * @return list of games that player take place in
	 */
	public List<GameTo> showGames(Long userId);
}
