package com.capgemini.chess.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.chess.dao.GameDao;
import com.capgemini.chess.dao.StatisticDao;
import com.capgemini.chess.model.enumerator.Level;
import com.capgemini.chess.model.enumerator.ScaleBase;
import com.capgemini.chess.model.mapper.GameMapper;
import com.capgemini.chess.model.mapper.StatisticMapper;
import com.capgemini.chess.model.to.GameTo;
import com.capgemini.chess.model.to.StatisticTo;
import com.capgemini.chess.model.to.UserTo;
import com.capgemini.chess.service.MatchAndStatisticManageService;
import com.capgemini.chess.service.UserManageService;

@Service
@Transactional
public class MatchAndStatisticManageServiceImpl implements MatchAndStatisticManageService {

	@Autowired
	private GameDao gameDao;

	@Autowired
	private StatisticDao statisticDao;

	@Autowired
	private UserManageService userService;

	@Override
	public void saveMatchAndUpdateStatistic(Long winnerId, Long loserId) {
		GameTo game = saveMatch(winnerId, loserId);
		StatisticTo winStat = userService.find(winnerId).getStatistic();
		StatisticTo losserStat = userService.find(loserId).getStatistic();
		setStatistic(winStat, losserStat, game);
		statisticDao.update(StatisticMapper.map(winStat));
		statisticDao.update(StatisticMapper.map(losserStat));
		setAllUsersRankingPosition();
	}

	@Override
	public void setAllUsersRankingPosition() {
		Map<Level, Long> lastUsedPositionOnLevel = new HashMap<>();
		List<StatisticTo> usersStats = StatisticMapper.map2To(statisticDao.getSortedListOfPlayers());
		long rankingPosition = 1L;
		Level tempLevel = null;
		for (StatisticTo statisticTo : usersStats) {
			tempLevel = statisticTo.getLevel();
			statisticTo.setPositionInRanking(rankingPosition++);
			long positionOnLevel = getLastPositionOnLevel(tempLevel, lastUsedPositionOnLevel);
			statisticTo.setPositionOnLevel(positionOnLevel);
			statisticDao.update(StatisticMapper.map(statisticTo));
		}
	}

	private long getLastPositionOnLevel(Level level, Map<Level, Long> positions) {
		if (positions.containsKey(level)) {
			long position = positions.get(level).longValue();
			positions.put(level, ++position);
			return position;
		} else {
			positions.put(level, 1L);
			return 1L;
		}
	}

	private GameTo saveMatch(Long winnerId, Long loserId) {
		UserTo winner = userService.find(winnerId);
		UserTo losser = userService.find(loserId);
		GameTo game = getMatchTo(winner, losser);
		gameDao.save(GameMapper.map(game));
		return game;
	}

	private void setStatistic(StatisticTo winStat, StatisticTo loserStat, GameTo game) {
		updateWinnerGameCount(winStat);
		updateLoserGameCount(loserStat);
		updatePoints(winStat, loserStat, game);
		updateLevel(winStat);
		updateLevel(loserStat);
	}

	private GameTo getMatchTo(UserTo winner, UserTo loser) {
		GameTo game = new GameTo();
		game.setLoser(loser);
		game.setWinner(winner);
		game.setWinnerLevel(winner.getStatistic().getLevel());
		game.setLoserLevel(loser.getStatistic().getLevel());
		game.setScaleBase(getScaleBase(game.getWinnerLevel(), game.getLoserLevel()));
		return game;
	}

	private ScaleBase getScaleBase(Level winnerLevel, Level loserLevel) {
		int scaleBaseIndex = loserLevel.getLevelData()[0] - winnerLevel.getLevelData()[0];
		return ScaleBase.getScaleBaseByNumber(scaleBaseIndex);
	}

	private void updateLoserGameCount(StatisticTo losserStat) {
		long lostGames = losserStat.getMatchesLost();
		long playedGames = losserStat.getMatchesPlayed();
		losserStat.setMatchesLost(++lostGames);
		losserStat.setMatchesPlayed(++playedGames);
	}

	private void updateWinnerGameCount(StatisticTo winStat) {
		long wonGames = winStat.getMatchesWon();
		long playedGames = winStat.getMatchesPlayed();
		winStat.setMatchesWon(++wonGames);
		winStat.setMatchesPlayed(++playedGames);
	}

	private void updatePoints(StatisticTo winStat, StatisticTo loserStat, GameTo game) {
		long winerActualPoints = winStat.getPoints();
		long losserActualPoints = loserStat.getPoints();
		long baseProfit = getWinnerBase(winStat, game);
		long baseLoss = getLoserBase(loserStat, game);
		double loserProgres = getProgress(loserStat);
		double winnerProgress = getProgress(winStat);
		long winnerBonus = (long) Math.floor((loserProgres - winnerProgress) * baseProfit * 0.5);
		long loserBonus = (long) Math.floor((winnerProgress - loserProgres) * baseProfit * 0.5);
		long profit = baseProfit + winnerBonus;
		long loss = baseLoss - loserBonus;
		long updatedLoserPoints = (losserActualPoints - loss) > 0L ? (losserActualPoints - loss) : 0L;
		winStat.setPoints(winerActualPoints + profit);
		loserStat.setPoints(updatedLoserPoints);
	}

	private int getWinnerBase(StatisticTo stats, GameTo game) {
		return game.getScaleBase().getBase()[1] * stats.getLevel().getLevelData()[0];
	}

	private int getLoserBase(StatisticTo stats, GameTo game) {
		return game.getScaleBase().getBase()[2] * stats.getLevel().getLevelData()[0];
	}

	private double getProgress(StatisticTo statistic) {
		if (statistic.getLevel() == Level.CHUCK_NORRIS_OF_CHESS) {
			return 0.0;
		}
		double progressPoints = getProgressPoints(statistic);
		double progressGames = getProgressGames(statistic);
		double progressWins = getProgressWins(statistic);
		return (progressGames + progressPoints + progressWins) / 3;
	}

	private double getProgressGames(StatisticTo statistic) {
		double games = statistic.getMatchesPlayed();
		int level = statistic.getLevel().getLevelData()[0];
		int gamesOnPlayerLevel = Level.findLevel(level).getLevelData()[2];
		double progressGames = (games - gamesOnPlayerLevel)
				/ (Level.findLevel(level + 1).getLevelData()[2] - gamesOnPlayerLevel);
		return Math.min(progressGames, 1.0);
	}

	private double getProgressPoints(StatisticTo statistic) {
		double points = statistic.getPoints();
		int level = statistic.getLevel().getLevelData()[0];
		int pointsOnPlayerLevel = Level.findLevel(level).getLevelData()[1];
		double progressPoints = (points - pointsOnPlayerLevel)
				/ (Level.findLevel(level + 1).getLevelData()[1] - pointsOnPlayerLevel);
		return Math.min(progressPoints, 1.0);
	}

	private double getProgressWins(StatisticTo statistic) {
		double wins = statistic.getMatchesWon();
		int level = statistic.getLevel().getLevelData()[0];
		int gamesOnPlayerLevel = Level.findLevel(level).getLevelData()[3];
		double progressWins = (wins - gamesOnPlayerLevel)
				/ (Level.findLevel(level + 1).getLevelData()[3] - gamesOnPlayerLevel);
		return Math.min(progressWins, 1.0);
	}

	private void updateLevel(StatisticTo statistic) {
		long games = statistic.getMatchesPlayed();
		long wonGames = statistic.getMatchesWon();
		long points = statistic.getPoints();
		statistic.setLevel(Level.NEWBIE);
		setLevel(statistic, games, wonGames, points);
	}

	private void setLevel(StatisticTo statistic, long games, long wonGames, long points) {
		for (Level level : Level.values()) {
			long requirePoints = level.getLevelData()[1];
			long requireMatches = level.getLevelData()[2];
			int requireWins = level.getLevelData()[3];
			long percentWins = (long) (((double) wonGames / statistic.getMatchesPlayed()) * 100);
			if (points >= requirePoints && percentWins >= requireWins && games >= requireMatches) {
				statistic.setLevel(level);
			}
		}
	}
}
